* Hyperfine: CLI benchmarks

#+FILETAGS: :tool:benchmark

https://github.com/sharkdp/hyperfine

Hyperfine can be used to benchmark any CLI invocation

Hyperfine advise to drop disk caches before running I/O benchmarks:
#+begin_src
  sudo -v 
  sync; echo 3 | sudo tee /proc/sys/vm/drop_caches
#+end_src

Usefully for  [[file:2022-11-03-0941-Go-Benchamrking.org][go benchmarks]] too


