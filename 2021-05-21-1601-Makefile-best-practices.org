* Makefile best practices

#+FILETAGS: :make:devops

[[https://tech.davis-hansson.com/p/make/][Your makefiles are wrong]]

Les =Makefile= sont une bonne manière de définir comment lancer/tester/déployer un projet. Ils
contiennent plusieurs pièges. Voici plusieurs chose a changer pour s'assurer que ces fichiers
restent corrects et compréhensibles.

** Pas de tabulation 
=make= utilise normalement des tab pour définir des recettes. Mixer des tabs et des espaces dans un
environnent de shell n'est jamais une très bonne idée. On peut donc remplacer les tabs par un autre
séparateur tel que =>= 
#+begin_src makefile
  ifeq ($(origin .RECI	PEPREFIX), undefined)
    $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
  endif
  .RECIPEPREFIX = >  
#+end_src
On ajoute également un petit check pour se protéger contre des version de =make= trop vielles 

** Utiliser un shell précis
Par défaut =make= utilise =/bin/sh=. Plutôt que d'essayer de rester portable entre tout les shells, on
peut fixer le shell utilisé
#+begin_src makefile
  SHELL := bash  	
#+end_src

** Utiliser un mode strict de bash 
On ne veut pas qu'une erreur dans un script passe inaperçue
#+begin_src makefile
  .SHELLFLAGS := -eu -o pipefail -c  
#+end_src

** Modification de certain défauts 
Utilise un shell par recette plutôt qu'un shell par ligne:
#+begin_src makefile
  .ONESHELL
#+end_src

Supprime la cible d'une recette lors d'un échec de celle ci:
#+begin_src makefile 
  .DELETE_ON_ERROR
#+end_src

Avertissement si variable non définie:
#+begin_src makefile
  MAKEFLAGS += --warn-undefined-variables
#+end_src

Suppression des règles automatiques:
#+begin_src makefile
  MAKEFLAGS += --no-builtin-rules
#+end_src

** Filesystem, règles PHONY et fichier sentinelle 
Si une recette ne crée pas de le de fichier avec son propre nom, il faut faire attention à la
marquer comme étant une recette =.PHONY=. Cela empêche sa désactivation si un fichier du mémé nom
existe. 

Néanmoins faire cela désactive la capacité de =make= a ne reconstruire que les recettes périmées. Si
on veut garder cette capacité, il faut utiliser des fichiers sentinelles qui serviront de marqueur
temporel pour déterminer l'état de la recette. 
#+begin_src makefile 
  out/.packed.sentinel: $(shell find src -type f)
  > node run webpack ..	
  > touch out/.packed.sentinel
#+end_src

