* Cache sans TTL

#+FILETAGS: :programming:cache:

[[http://calpaterson.com/ttl-hell.html]]

Il est difficile de bien choisir le =TTL= (Time-to-Live) d'un élément mis en cache. Voici
différentes stratégies pour résoudre ce problème

** Pas de TTL
Utiliser un cache =LRU=, ie qui supprime automatiquement les vieux éléments. Utile si les
données sont toujours valides.

** Update-on-Write
Toujours pas de TTL, mais on met a jour l'entrée de cache lorsque la donnée est
modifiée. Le cache contient donc toujours des données valides. Problème si des données
changent mais qu'on ne les connais pas toutes

** Invalidate-on-Write
On supprime l'entrée du cache quand les données ne sont plus valide. On a pas besoin de
connaître toutes les nouvelles données pour faire cela, et les données seront régénérées
lors de la prochaine requête au cache échoué

** Namespacing
Le problème de la méthode précédente est qu'il faut connaître toute les clés à invalider.
On peut utiliser une paire de clés pour facilement invalider toutes les données 

Exemple :
- */namespace/user657* -> Compteur monotonique
- */user657/12345/key1* 
- */user657/12345/key2* 
- */user657/12345/key3* 

Un incrément du compteur rendra toutes les données précédentes invalides. Mais il faut
maintenant deux requêtes au cache pour obtenir les données (une pour le compteur et une
pour la valeur)

** Erreurs classiques 

*** Mettre des données uniquement dans le cache 
Si on utilise le cache pour mettre des données importantes, on risque de les perdre en
mode =LRU=. Ex: Structures de données redis. Si on veut utiliser redis pour ses structures
de données, alors on ne doit plus le considérer comme un cache. Il faudrait utiliser deux
instances redis, une comme un cache =LRU= et l'autre en tant que file de communication
inter-process.

*** Ne jamais requérir des données dans le cache 
Si on ne trouve pas des données dans le cache, il faut pourvoir les retrouver en base de
donnée 

*** Ne pas cacher ce qu'on ne peut pas invalider 
