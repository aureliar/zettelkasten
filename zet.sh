#! /usr/bin/env bash
#Zettelkasten method 
set -e

ZROOT=${ZET_ROOT:=~/personnal/zettelkasten}

name="$ZROOT/$(date +"%Y-%m-%d-%H%M")"
content="*"

for word in $@; do
  name+="-$word"
  content+=" ${word}"
done

if [ ! -e "$name.org" ]; then
  echo -e "$content\n\n#+FILETAGS:" > $name.org
fi

emacsclient -n $name.org
