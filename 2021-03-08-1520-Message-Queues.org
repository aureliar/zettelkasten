* Message Queues

 #+FILETAGS: :programming:architecture:

[[https://sudhir.io/the-big-little-guide-to-message-queues/]]

L'objectif est de découpler le système qui émet des message et celui qui les reçoit.

#+begin_quote
If predictability is more important than throughput, we're better off without a queue.
#+end_quote>

** Réception des messages 
Il existe trois façons de recevoir des messages:
- Au moins une fois
- Au maximum une fois
- Exactement une fois

Exactement une fois est impossible ([[https://lobste.rs/s/ecjfcm/why_is_exactly-once_messaging_not_possible_in_a_distributed_queue][source]]). Il faut donc s'assurer de pouvoir supporter Au moins
une fois. Pour cela, il faut s'assurer qu'on est capable de traiter plusieurs le même message sans
soucis.

** Ordre de traitement 
On peut choisir de recevoir les message dans l'ordre ou de s'autoriser a les traiter en
parallèle. Demander de garder un ordre est cher et va réduire le débit maximal de la file. 
Il vaut dont mieux designer le système pour s'autoriser à traiter les messages dans le mauvais ordre
(ex: Rajouter des timestamps dans les messages)

** Traitement des messages invalides 
Lorsqu'un message ne peut pas être traité par le récepteur, il choisir entre ne jamais l'acquitter
et le supprimer. Ou alors le système de queue peut déplacer le message dans une dead queue après
suffisamment d'échec. 
